/*
 * Created by rogergcc
 * Copyright Ⓒ 2019 . All rights reserved.
 */

package com.rogergcc.sharedpreferencefavorites.helpers;

public class Constants {
    //public static final int WALLET_ENVIRONMENT = WalletConstants.ENVIRONMENT_TEST;


    public static final String SHARED_PREF = "shared_pref";
    public static final String FAVORITE_COUNT = "favorite_count";
    public static final String FAVORITE_ID = "favorite_id";
    public static final String WEBSERVICES_URL = "http:/...";



}
