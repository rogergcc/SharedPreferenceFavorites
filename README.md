# SharedPreferenceFavorites
 Save you collections offline in android — sharepreferences
https://medium.com/@rogercolquehuanca/storage-data-offline-in-android-sharepreferences-39fd34a0b7ff
 ## 📸 Screenshots

**Click the image below to enlarge.**


<div>
 
<img src="https://github.com/rogergcc/SharedPreferenceFavorites/blob/master/screenshots/screenshot-1572635453673.jpg" height="480" width="270" hspace="10">

<img src="https://github.com/rogergcc/SharedPreferenceFavorites/blob/master/screenshots/screenshot-1572666815800.jpg" height="480" width="270" hspace="10">


<img src="https://github.com/rogergcc/SharedPreferenceFavorites/blob/master/screenshots/screenshot-1572666933983.jpg" height="480" width="270" hspace="10">
</div>
